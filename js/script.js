const arr = ['hello', 'world', 23, '23', null, 0, {}, true,];
const type = prompt('Enter type of (string, number, bigint, object, boolean');

function filterBy(array, value) {
  return array.filter(function (item) {
    return typeof item !== value;
  });
}

console.log(filterBy(arr, type));